import { Selector } from 'testcafe';
import { login } from './test'

fixture `testcafe_poc`
    .page `http://cyberdyne.lvh.me:3000/users/sign_in`
    .beforeEach(login)

test('Create app Test Cafe Poc', async t => {
    await t
        .click(Selector('button').withText('add'))
        .click(Selector('span').withText('Create App'))
        .typeText(Selector('#name'), 'New Poc Test Cafe003')
        .click(Selector('button').withText('Create'))
        .click(Selector('label').withText('Fields'))
        .click(Selector('button').withText('Add'))
        .doubleClick(Selector('#name'), {
            caretPos: 0
        })
        .pressKey('ctrl+a delete')
        .typeText(Selector('#name'), 'attachment')
        .pressKey('tab')
        .click(Selector('#data_type'), {
            caretPos: 0
        })
        .pressKey('ctrl+a delete')
        .typeText(Selector('#data_type'), 'attachment')
        .pressKey('enter')
        .click(Selector('button').withText('Add'))
        .click(Selector('#name'))
        .pressKey('ctrl+a delete')
        .typeText(Selector('#name'), 'document_folder')
        .pressKey('tab')
        .click(Selector('#data_type'), {
            caretPos: 0
        })
        .pressKey('ctrl+a delete')
        .typeText(Selector('#data_type'), 'DocumentFolder')
        .pressKey('enter')
        .click(Selector('button').withText('Save'))
        .wait(3);
});


test('Create app Test Cafe Poc 2', async t => {
    await t
        .click(Selector('button').withText('add'))
        .click(Selector('span').withText('Create App'))
        .typeText(Selector('#name'), 'New Poc Test Cafe004')
        .click(Selector('button').withText('Create'))
        .click(Selector('label').withText('Fields'))
        .click(Selector('button').withText('Add'))
        .doubleClick(Selector('#name'), {
            caretPos: 0
        })
        .pressKey('ctrl+a delete')
        .typeText(Selector('#name'), 'attachment')
        .pressKey('tab')
        .click(Selector('#data_type'), {
            caretPos: 0
        })
        .pressKey('ctrl+a delete')
        .typeText(Selector('#data_type'), 'attachment')
        .pressKey('enter')
        .click(Selector('button').withText('Add'))
        .click(Selector('#name'))
        .pressKey('ctrl+a delete')
        .typeText(Selector('#name'), 'document_folder')
        .pressKey('tab')
        .click(Selector('#data_type'), {
            caretPos: 0
        })
        .pressKey('ctrl+a delete')
        .typeText(Selector('#data_type'), 'DocumentFolder')
        .pressKey('enter')
        .click(Selector('button').withText('Save'))
        .wait(3);
});
